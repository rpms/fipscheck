#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/fipscheck/Sanity/fipshmac
#   Description: Test fipshmac helper tool.
#   Author: Ondrej Moris <omoris@redhat.com>
# 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="fipscheck"

rlJournalStart

    rlPhaseStartSetup

        rlCheckRpm "fipscheck" || rlDie

        testdir=$(mktemp -d)
        testpath=$(mktemp)
        testfile=$(basename $testpath)
        mountdir=$(mktemp -d)

        rlRun "useradd testuser" 0
        rlRun "mount -t tmpfs -o size=512k tmpfs $mountdir" 0
        rlRun "dd if=/dev/urandom bs=512k count=1 of=${mountdir}/blob" 0
        
    rlPhaseEnd

    rlPhaseStartTest "HMAC generation"

        # Create HMAC.
        rlRun "echo 'abc' > $testpath" 0
        rlRun "fipshmac $testpath" 0

        # Create HMAC in specified directory.
        rlRun "fipshmac -d $testdir $testpath" 0

        # Create HMAC with a specific suffix.
        rlRun "fipshmac -s .suffix $testpath" 0
        
        # Check that HMAC files exist.
        rlAssertExists "/tmp/.${testfile}.hmac"
        rlAssertExists "/tmp/.${testfile}.suffix"
        rlAssertExists "$testdir/${testfile}.hmac"

        # Verify HMAC.
	rlRun "fipscheck $testpath" 0

        # Check that HMAC file is not empty.
        rlRun "test -s /tmp/.${testfile}.hmac" 0

        # Check that HMAC file does not change when recomputed.
        old=$(cat /tmp/.${testfile}.hmac)
        rlRun "fipshmac $testpath" 0
        new=$(cat /tmp/.${testfile}.hmac)
        rlAssertEquals "Hashes should not differ" "$old" "$new"

        # Check that HMAC file changes when recomputed and file changed.
        old=$(cat /tmp/.${testfile}.hmac)
        rlRun "echo 'efg' >> $testpath" 0
        rlRun "fipshmac $testpath" 0
        new=$(cat ./.test.hmac)
        rlAssertNotEquals "Hashes should differ" "$old" "$new"
        
    rlPhaseEnd

    rlPhaseStartTest "Return Codes"

       # 0: OK.
       rlLog "0: OK"
       rlRun "fipshmac $testpath" 0
             
       # 2: Missing filename.
       rlLog "2: Missing filename"
       rlRun "fipshmac" 2

       # 3: Cannot open the checksum file for writing.
       rlLog "3: Cannot open the checksum file for writing"
       rlRun "chmod a-r $testpath" 0
       rlRun "runuser -u testuser fipshmac $testpath" 3
       rlRun "chmod a+r $testpath" 0

       # 4: Cannot read the file to be checksummed.
       rlLog "4: Cannot read the file to be checksummed"
       rlRun "fipshmac missing" 4
       
       # 5: Memory allocation error
       # N/A
       
       # 6,7: Cannot write to the checksum file.
       rlLog "6,7: Cannot write to the checksum file"
       rlRun "fipshmac -d $mountdir $testpath" 6,7
       
    rlPhaseEnd

    rlPhaseStartCleanup

        rlRun "rm -rf $testpath .${testfile}.hmac $testdir" 0
        rlRun "userdel testuser" 0
        rlRun "umount $mountdir" 0

    rlPhaseEnd

rlJournalPrintText

rlJournalEnd
