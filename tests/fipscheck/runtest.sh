#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/fipscheck/Sanity/fipscheck
#   Description: Test fipscheck helper tool.
#   Author: Ondrej Moris <omoris@redhat.com>
# 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="fipscheck"

rlJournalStart

    rlPhaseStartSetup

        rlCheckRpm "fipscheck" || rlDie
        rlCheckRpm "fipscheck-lib" || rlDie

        tmpdir=$(mktemp -d)

        rlRun "chmod a+rwx $tmpdir" 0
        rlRun "pushd $tmpdir" 0
        
        echo 'hmac' >hmac
        echo 'suffixhmac' >suffixhmac
        echo 'incorrecthmac' >incorrecthmac
        echo 'emptyhmac' >emptyhmac
        
        rlRun "fipshmac hmac" 0
        rlRun "fipshmac -s .suffix suffixhmac" 0
        rlRun "fipshmac incorrecthmac && echo 'X' >.incorrecthmac.hmac" 0
        rlRun "fipshmac emptyhmac && echo '' >.emptyhmac.hmac" 0
        
        fipscheck_hmac=$(rpm -ql fipscheck | egrep '\.hmac')
        fipscheck_libs_all=$(rpm -ql fipscheck-lib | grep so | grep -v hmac | tr '\n' ' ')
        fipscheck_libs_some_hmac=$(rpm -ql fipscheck-lib | egrep '\.hmac' | head -1)

        rlRun "useradd testuser" 0

    rlPhaseEnd

    rlPhaseStartTest "Integrity"

        rlCheckRpm "prelink" && rlRun "prelink -ua" 0
        rlRun "fipscheck /usr/bin/fipscheck $fipscheck_libs_all" 0

    rlPhaseEnd
        
    rlPhaseStartTest "HMAC verification"

        # Verify HMAC.
        rlRun "fipscheck hmac" 0

        # Verify HMAC with specific suffix.
        rlRun "fipscheck -s .suffix suffixhmac" 0

        # Incorrect HMAC causes failure.
        rlRun "fipscheck incorrecthmac" 1
        
        # Empty HMAC causes failure.
        rlRun "fipscheck emptyhmac" 1
                
    rlPhaseEnd

    rlPhaseStartTest "Return Codes"
    
        # 0: Checksum OK.
        rlLog "0: Checksum OK"
        rlRun "fipscheck hmac" 0
       
        # 1: Checksum mismatch.
        rlLog "1: Checksum mismatch"
        rlRun "fipscheck incorrecthmac" 1

        # 2: Missing filename.
        rlLog "2: Missing filename"
        rlRun "fipscheck" 2
        
        # 3: Cannot open the checksum file.
        rlLog "3: Cannot open the checksum file"
        rlRun "fipscheck runtest.sh" 3
        
        # 4: Cannot read the file to be checksummed.
        rlLog "4: Cannot read the file to be checksummed"
        rlRun "chmod a-r hmac" 0
        rlRun "runuser -u testuser fipscheck hmac" 4
        rlRun "chmod a+r hmac" 0

        # 5: Memory allocation error.
        # N/A

        # 10 and higher: Failure during self-checking the libfipscheck.so shared library.
        rlLog "10 and higher: Failure during self-checking the libfipscheck.so shared library"
        rlRun "mv $fipscheck_libs_some_hmac ${fipscheck_libs_some_hmac}.backup" 0
        rlRun "fipscheck hmac" 10-19
        rlRun "mv ${fipscheck_libs_some_hmac}.backup $fipscheck_libs_some_hmac" 0
        
        # 20 and higher: Failure during self-checking the fipscheck binary.
        rlRun "mv $fipscheck_hmac ${fipscheck_hmac}.backup" 0
        rlRun "fipscheck hmac" 20-255
        rlRun "mv ${fipscheck_hmac}.backup $fipscheck_hmac" 0
           
    rlPhaseEnd

    rlPhaseStartCleanup

        rlRun "popd" 0
        rlRun "rm -rf $tmpdir" 0
        rlRun "userdel testuser" 0

    rlPhaseEnd

rlJournalPrintText

rlJournalEnd
