#include <stdio.h>
#include <stdlib.h>
#include <fipscheck.h>
#include <dlfcn.h>
#include <getopt.h>
#include <string.h>

int main(int argc, char *argv[]) {

    static struct option long_options[] = {            
        { "verify", optional_argument, 0, 1 },
        { "verify-ex", optional_argument, 0, 2 },
        { "verify-files", required_argument, 0, 3 },
        { "verify-files-ex", required_argument, 0, 4 },
        { "fips-module-installed", optional_argument, 0, 5 },
        { "kernel-fips-mode", no_argument, 0, 6 },
        {0, 0, 0, 0}
    };
    
    int rc = 0, option_index = 0, fail_if_missing;
    int c = getopt_long (argc, argv, "", long_options, &option_index);
    char *libname = NULL, *symbolname = NULL, *suffix = NULL, *files[3];
    
    switch (c) {

    case 1:
        if (optarg != NULL) {            
            libname = strtok(optarg, ",");
            symbolname = strtok(NULL, ",");
        }        
        rc = FIPSCHECK_verify(libname, symbolname);
        break;

    case 2:
        if (optarg != NULL) {            
            libname = strtok(optarg, ",");
            symbolname = strtok(NULL, ",");
            suffix = strtok(NULL, ",");
            fail_if_missing = atoi(strtok(NULL, ","));
        }
#ifndef RHEL6
        rc = FIPSCHECK_verify_ex(libname, symbolname, suffix, fail_if_missing);
#endif
        break;

    case 3:
        files[0] = strtok(optarg, ",");
        files[1] = strtok(NULL, ",");
        files[2] = NULL;
        rc = FIPSCHECK_verify_files((const char **) files);
        break;

    case 4:
        suffix = strtok(optarg, ",");
        fail_if_missing = atoi(strtok(NULL, ","));
        files[0] = strtok(NULL, ",");
        files[1] = strtok(NULL, ",");
        files[2] = NULL;
#ifndef RHEL6
        rc = FIPSCHECK_verify_files_ex(suffix, fail_if_missing, (const char **) files);
#endif
        break;
        
    case 5:
        if (optarg != NULL) {            
            libname = strtok(optarg, ",");
            symbolname = strtok(NULL, ",");
            suffix = strtok(NULL, ",");
        }        
#ifndef RHEL6
        rc = FIPSCHECK_fips_module_installed(libname, symbolname, suffix);
#endif
        break;

    case 6:
        rc = FIPSCHECK_kernel_fips_mode();
        break;
    }
    
    return rc;
}
