#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/fipscheck/Sanity/library
#   Description: Test fipscheck API
#   Author: Ondrej Moris <omoris@redhat.com>
# 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

#PACKAGE="fipscheck"
rlJournalStart

    rlPhaseStartSetup

        rlCheckMakefileRequires || rlDie
    
        # Make sure nothing is prelinked (as expected in FIPS).
        rlCheckRpm "prelink" && rlRun "prelink -ua" 0

        rlIsRHEL 6 && debug="-DRHEL6"
	rlRun "gcc $debug test.c -o test -lfipscheck -ldl"

    rlPhaseEnd

    rlPhaseStartTest "FIPSCHECK_verify()"

        rlLog "Function should return 1 if checksum is valid for a given"
        rlLog "library (or a current process), 0 otherwise."
        
        # TC1
        fipshmac "test" 
        rlRun "./test --verify" 1 \
              "TC1: process, correct checksum, should return 1"
        rm -f ".test.hmac" 

        # TC2
        echo 'abc' > ".test.hmac" 
        rlRun "./test --verify" 0 \
              "TC2: process, incorrect checksum, should return 0"
        rm -f ".test.hmac" 

        # TC3
        rm -f ".test.hmac" 
        rlRun "./test --verify" 0 \
              "TC3: process, missing checksum, should return 0"

        # Setup for TC4 - TC8.
        hmac=$(rpm -ql libgcrypt | grep hmac | head -1)
        name=$(echo $hmac | sed 's|^.*/\.\(.\+\).hmac|\1|')
        path=$(echo $hmac | sed 's|^\(.\+\)/\..*$|\1/|')
        file=$(rpm -ql libgcrypt | egrep "/${name}\$")
        symb=$(objdump -T $file | grep .text | awk '{print $7}' | tail -1)
        echo "hmac=$hmac"
        echo "name=$name"
        echo "path=$path"
        echo "file=$file"
        echo "symb=$symb"

        # TC4
        rlRun "LD_PRELOAD=$file ./test --verify=${file},${symb}" 1 \
              "TC4: library, correct checksum, should return 1"
        
        # TC5
        temp=$(mktemp)
        mv $hmac $temp
        echo 'abc' >$hmac
        rlRun "LD_PRELOAD=$file ./test --verify=${file},${symb}" 0 \
              "TC5: library, incorrect checksum, should return 0"
        mv $temp $hmac

        # TC6
        temp=$(mktemp)
        mv $hmac $temp
        rlRun "LD_PRELOAD=$file ./test --verify=${file},${symb}" 0 \
              "TC6: library, missing checksum, should return 0"
        mv $temp $hmac
        
        # TC7
        rlRun "LD_PRELOAD=$file ./test --verify=${file}x,${symb}" 0 \
              "TC7: library, incorrect library, should return 0"

        # TC8
        rlRun "LD_PRELOAD=$file ./test --verify=${file},${symb}x" 0 \
              "TC8: library, incorrect symbol, should return 0"
        
    rlPhaseEnd

    if !rlIsRHEL 6; then
    rlPhaseStartTest "FIPSCHECK_verify_ex()"

        rlLog "Function should return 1 if checksum is valid for a current"
        rlLog "process or a given library and suffix (if fail_is_missing"
        rlLog "is set to 1), 0 otherwise."

        # TC1
        fipshmac "test" 
        rlRun "./test --verify-ex" 1 \
              "TC1: process, correct checksum, should return 1"
        rm -f ".test.hmac" 

        # TC2
        echo 'abc' > ".test.hmac" 
        rlRun "./test --verify-ex" 0 \
              "TC2: process, incorrect checksum, should return 0"
        rm -f ".test.hmac" 

        # TC3
        rm -f ".test.hmac" 
        rlRun "./test --verify-ex" 1 \
              "TC3: process, missing checksum (pass if missing), should return 1"

        # Setup for TC4 - TC10.
        hmac=$(rpm -ql libgcrypt | grep hmac | head -1)
        name=$(echo $hmac | sed 's|^.*/\.\(.\+\).hmac|\1|')
        path=$(echo $hmac | sed 's|^\(.\+\)/\..*$|\1/|')
        file=$(rpm -ql libgcrypt | egrep "/${name}\$")
        symb=$(objdump -T $file | grep .text | awk '{print $7}' | tail -1)
        echo "hmac=$hmac"
        echo "name=$name"
        echo "path=$path"
        echo "file=$file"
        echo "symb=$symb"

        # TC4
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file},${symb},.hmac,0" 1 \
              "TC4: library, correct checksum, should return 1"
        
        # TC5
        temp=$(mktemp)
        mv $hmac $temp
        echo 'abc' >$hmac
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file},${symb},.hmac,0" 0 \
              "TC5: library, incorrect checksum, should return 0"
        mv $temp $hmac

        # TC6
        temp=$(mktemp)
        mv $hmac $temp
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file},${symb},.hmac,0" 1 \
              "TC6: library, missing checksum (pass is missing), should return 1"
        mv $temp $hmac

        # TC7
        temp=$(mktemp)
        mv $hmac $temp
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file},${symb},.hmac,1" 0 \
              "TC7: library, missing checksum (fail is missing), should return 0"
        mv $temp $hmac
        
        # TC8
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file}x,${symb},.hmac,1" 0 \
              "TC8: library, incorrect library, should return 0"

        # TC9
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file},${symb}x,.hmac,1" 0 \
              "TC9: library, incorrect symbol, should return 0"

        # TC10
        rlRun "LD_PRELOAD=$file ./test --verify-ex=${file},${symb},.hmacx,1" 0 \
              "TC10: library, incorrect suffix, should return 0"
        
    rlPhaseEnd
    fi
    
    rlPhaseStartTest "FIPSCHECK_verify_files()"

        rlLog "Function should return 1 if checksum is valid for given files"
        rlLog "otherwise."

        # Setup for TC1 - TC5.
        hmac1=$(rpm -ql libgcrypt | grep hmac | head -1)
        name1=$(echo $hmac1 | sed 's|^.*/\.\(.\+\).hmac|\1|')
        path1=$(echo $hmac1 | sed 's|^\(.\+\)/\..*$|\1/|')
        file1=$(rpm -ql libgcrypt | egrep "/${name1}\$")
        echo "hmac1=$hmac1"
        echo "name1=$name1"
        echo "path1=$path1"
        echo "file1=$file1"

        hmac2=$(rpm -ql libreswan | grep pluto.hmac | head -1)
        name2=$(echo $hmac2 | sed 's|^.*/\(.\+\).hmac|\1|')
        path2=$(echo $hmac2 | sed 's|^\(.\+\)/.*$|\1/|')
        file2=$(rpm -ql libreswan | egrep "ipsec/${name2}\$")
        echo "hmac2=$hmac2"
        echo "name2=$name2"
        echo "path2=$path2"
        echo "file2=$file2"
        
        # TC1
        rlRun "./test --verify-files ${file1},${file2}" 1 \
              "TC1: library, correct checksums, should return 1"
        
        # TC2
        temp1=$(mktemp)
        mv $hmac1 $temp1
        echo 'abc' >$hmac1
        rlRun "./test --verify-files=${file1},${file2}" 0 \
              "TC2: library, correct and incorrect checksums, should return 0"
        mv $temp1 $hmac1

        # TC3
        temp1=$(mktemp)
        temp2=$(mktemp)
        mv $hmac1 $temp1
        mv $hmac2 $temp2
        echo 'abc' >$hmac1
        echo 'abc' >$hmac2
        rlRun "./test --verify-files=${file1},${file2}" 0 \
              "TC3: library, incorrect checksums, should return 0"
        mv $temp1 $hmac1
        mv $temp2 $hmac2
        
        # TC4
        temp1=$(mktemp)
        mv $hmac1 $temp1
        rlRun "./test --verify-files=${file1},${file2}" 0 \
              "TC4: library, one checksum missing, should return 0"
        mv $temp1 $hmac1

        # TC5
        temp1=$(mktemp)
        temp2=$(mktemp)
        mv $hmac1 $temp1
        mv $hmac2 $temp2
        rlRun "./test --verify-files=${file1},${file2}" 0 \
              "TC5: library, both checksums missing, should return 0"
        mv $temp1 $hmac1
        mv $temp2 $hmac2
        
    rlPhaseEnd

    if !rlIsRHEL 6; then
    rlPhaseStartTest "FIPSCHECK_verify_files_ex()"

        rlLog "Function should return 1 if checksum is valid for given files"
        rlLog "and fail_if_missing is set to 1, 0 otherwise."
        
        # Setup for TC1 - TC8.
        hmac1=$(rpm -ql libgcrypt | grep hmac | head -1)
        name1=$(echo $hmac1 | sed 's|^.*/\.\(.\+\).hmac|\1|')
        path1=$(echo $hmac1 | sed 's|^\(.\+\)/\..*$|\1/|')
        file1=$(rpm -ql libgcrypt | egrep "/${name1}\$")
        echo "hmac1=$hmac1"
        echo "name1=$name1"
        echo "path1=$path1"
        echo "file1=$file1"

        hmac2=$(rpm -ql libreswan | grep hmac | head -1)
        name2=$(echo $hmac2 | sed 's|^.*/\(.\+\).hmac|\1|')
        path2=$(echo $hmac2 | sed 's|^\(.\+\)/.*$|\1/|')
        file2=$(rpm -ql libreswan | egrep "/${name2}\$")
        echo "hmac2=$hmac2"
        echo "name2=$name2"
        echo "path2=$path2"
        echo "file2=$file2"
        
        # TC1
        rlRun "./test --verify-files-ex .hmac,1,${file1},${file2}" 1 \
              "TC1: library, correct checksums, should return 1"
        
        # TC2
        temp1=$(mktemp)
        mv $hmac1 $temp1
        echo 'abc' >$hmac1
        rlRun "./test --verify-files-ex=.hmac,1,${file1},${file2}" 0 \
              "TC2: library, correct and incorrect checksums, should return 0"
        mv $temp1 $hmac1

        # TC3
        temp1=$(mktemp)
        temp2=$(mktemp)
        mv $hmac1 $temp1
        mv $hmac2 $temp2
        echo 'abc' >$hmac1
        echo 'abc' >$hmac2
        rlRun "./test --verify-files-ex=.hmac,1,${file1},${file2}" 0 \
              "TC3: library, incorrect checksums, should return 0"
        mv $temp1 $hmac1
        mv $temp2 $hmac2
        
        # TC4
        temp1=$(mktemp)
        mv $hmac1 $temp1
        rlRun "./test --verify-files-ex=.hnac,1,${file1},${file2}" 0 \
              "TC4: library, one checksum missing (fail if missing), should return 0"
        mv $temp1 $hmac1

        # TC5
        temp1=$(mktemp)
        mv $hmac1 $temp1
        rlRun "./test --verify-files-ex=.hnac,0,${file1},${file2}" 1 \
              "TC5: library, one checksum missing (pass if missing), should return 1"
        mv $temp1 $hmac1
        
        # TC6
        temp1=$(mktemp)
        temp2=$(mktemp)
        mv $hmac1 $temp1
        mv $hmac2 $temp2
        rlRun "./test --verify-files-ex=.hmac,1,${file1},${file2}" 0 \
              "TC6: library, both checksums missing (fail if missing), should return 0"
        mv $temp1 $hmac1
        mv $temp2 $hmac2

        # TC7
        temp1=$(mktemp)
        temp2=$(mktemp)
        mv $hmac1 $temp1
        mv $hmac2 $temp2
        rlRun "./test --verify-files-ex=.hmac,0,${file1},${file2}" 1 \
              "TC7: library, both checksums missing (pass if missing), should return 1"
        mv $temp1 $hmac1
        mv $temp2 $hmac2

        # TC8
        rlRun "./test --verify-files-ex=.hmacx,1,${file1},${file2}" 0 \
              "TC8: library, incorrect suffix, should return 0"
                
    rlPhaseEnd
    fi
    
    rlPhaseStartTest "FIPSCHECK_kernel_fips_mode()"

        rlLog "Function should return 1 if kernel is in FIPS mode, 0 otherwise."

        fips_mode=0
        grep 1 /proc/sys/crypto/fips_enabled && fips_mode=1
        rlRun "./test --kernel-fips-mode" $fips_mode

    rlPhaseEnd

    if !rlIsRHEL 6; then
    rlPhaseStartTest "FIPSCHECK_fips_module_installed()"

        rlLog "Function should return 1 if checksum for given library"
        rlLog "(or a current process) is available, 0 otherwise."

        # TC1
        touch ".test.hmac" 
        rlRun "./test --fips-module-installed" 1 \
              "TC1: process, checksum exists, should return 1"
        rm -f ".test.hmac" 

        # TC2
        rm -f ".test.hmac" 
        rlRun "./test --fips-module-installed" 0 \
              "TC2: process, checksum does not exist, should return 0"
        
        # Setup for TC3 - TC7.
        hmac=$(rpm -ql libgcrypt | grep hmac | head -1)
        name=$(echo $hmac | sed 's|^.*/\.\(.\+\).hmac|\1|')
        path=$(echo $hmac | sed 's|^\(.\+\)/\..*$|\1/|')
        file=$(rpm -ql libgcrypt | egrep "/${name}\$")
        symb=$(objdump -T $file | grep .text | awk '{print $7}' | tail -1)
        echo "hmac=$hmac"
        echo "name=$name"
        echo "path=$path"
        echo "file=$file"
        echo "symb=$symb"

        # TC3
        rlRun "LD_PRELOAD=$file ./test --fips-module-installed=${file},${symb},.hmac" 1 \
              "TC3: library, checksum exists, should return 1"
        
        # TC4
        temp=$(mktemp)
        mv $hmac $temp
        rlRun "LD_PRELOAD=$file ./test --fips-module-installed=${file},${symb},.hmac" 0 \
              "TC4: library, checksum does not exists, should return 0"
        mv $temp $hmac
       
        # TC5
        rlRun "LD_PRELOAD=$file ./test --fips-module-installed=${file}x,${symb},.hmac" 1 \
              "TC5: library, incorrect library, should return 1 (failsafe)"
        
        # TC6
        rlRun "LD_PRELOAD=$file ./test --fips-module-installed=${file},${symb}x,.hmac" 1 \
              "TC6: library, incorrect symbol, should return 1 (failsafe)"
               
        # TC7
        rlRun "LD_PRELOAD=$file ./test --fips-module-installed=${file},${symb},.hmacx" 0 \
              "TC7: library, incorrect suffix, should return 0"
        
    rlPhaseEnd
    fi
    
    rlPhaseStartCleanup
         rlRun "rm -f ./test" 0
    rlPhaseEnd

rlJournalPrintText

rlJournalEnd
